package br.com.fabricioandrade.filmesmvp.view.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import br.com.fabricioandrade.filmesmvp.common.enumeration.MoviesTabEnum
import br.com.fabricioandrade.filmesmvp.view.fragment.MoviesFragment

class PagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? {
        return when (position) {
            0 -> MoviesFragment.newInstance(MoviesTabEnum.ACAO.id,"")
            1 -> MoviesFragment.newInstance(MoviesTabEnum.DRAMA.id,"")
            2 -> MoviesFragment.newInstance(MoviesTabEnum.FANTASIA.id,"")
            3 -> MoviesFragment.newInstance(MoviesTabEnum.FICCAO.id,"")
            else -> null
        }
    }

    override fun getCount(): Int {
        return 4
    }
}
